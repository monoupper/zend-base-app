<?php

namespace Application\Controller;

use Application\Back\Map\Agregator;
use Application\Model\Coordinates;
use Application\Model\Repository\CoordinatesRepository;
use Zend\Http\Header\Referer;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractController
{

    /**
     * Index action
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel;
    }

    /**
     * Information action
     * @return ViewModel
     */
    public function informationAction()
    {
        return new ViewModel;
    }
}
