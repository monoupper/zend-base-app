<?php

namespace Application\Form;

use Application\Form\Element\Password;
use Zend\Form\Element\Email;
use Zend\Form\Form;

/**
 * Class Login
 * @package Application\Form
 */
class Login extends Form
{

    /**
     * @var null|int
     */
    private $identity;

    /**
     * Login constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct(null, $options);

        $this->add(
            [
                'type' => Email::class,
                'name' => 'email',
            ]
        );

        $this->add(
            [
                'type' => Password::class,
                'name' => 'password',
                'options' => [
                    'email'         => $this->getOption('email'),
                    'check'         => \Application\Form\Validator\Password::CHECK_LOGIN,
                    'form'          => $this
                ]
            ]
        );
    }

    public function getIdentity()
    {
        return $this->identity;
    }

    public function setIdentity($identity)
    {
        $this->identity = $identity;
    }

}