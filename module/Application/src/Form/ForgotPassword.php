<?php

namespace Application\Form;

use Application\Form\Element\Email;
use Application\Model\User;
use Zend\Form\Form;

/**
 * Class Login
 * @package Application\Form
 */
class ForgotPassword extends Form
{

    /**
     * Login constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        parent::__construct(null, $options);

        $this->add(
            [
                'type' => Email::class,
                'name' => 'email',
                'options' => [
                    'check_database' => User::class
                ]
            ]
        );
    }

}