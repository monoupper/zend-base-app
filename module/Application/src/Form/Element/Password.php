<?php

namespace Application\Form\Element;

use Zend\Form\Element;
use Zend\InputFilter\InputProviderInterface;
use Zend\Validator\StringLength;

/**
 * Class Password
 * @package Application\Form\Element
 */
class Password extends Element implements InputProviderInterface
{

    /**
     * @return array
     */
    public function getInputSpecification()
    {
        return [
            'name' => $this->getName(),
            'required' => true,
            'validators' => [
                new \Application\Form\Validator\Password(
                    [
                        'email' => $this->getOption('email'),
                        'check' => $this->getOption('check'),
                        'form'  => $this->getOption('form')
                    ]
                ),
                (new StringLength(['min' => 6, 'max' => 48]))
            ],
        ];
    }

}