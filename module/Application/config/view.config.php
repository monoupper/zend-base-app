<?php

namespace Application;

use Zend\ServiceManager\ServiceManager;

return [
    'factories' => [
        'route' => function (ServiceManager $serviceManager) {
            $route = new View\Helper\Route($serviceManager->get('Application')->getMvcEvent()->getRouteMatch());
            return $route;
        },
    ]
];