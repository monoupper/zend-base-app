<?php

namespace Application;

return [
    'factories'=> [
        'auth' => function(\Interop\Container\ContainerInterface $serviceManager) {
            $storage = new Auth\Storage('auth');
            $storage->setEntityManager($serviceManager->get('Doctrine\ORM\EntityManager'));

            $authService = new \Zend\Authentication\AuthenticationService();
            $authService->setStorage($storage);
            return $authService;
        },
        'mail' => function (\Zend\ServiceManager\ServiceManager $serviceManager) {
            $config = $serviceManager->get('config');
            $smtpOptions = $config['mail'] ?? [];
            $renderer = $serviceManager->get('Zend\View\Renderer\PhpRenderer');

            return new Mail\Sender($smtpOptions, $renderer);
        }
    ],
];