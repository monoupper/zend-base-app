<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\Router\Http\Segment;

return [
    'home' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index',
            ],
        ],
    ],
    'index' => [
        'type' => Segment::class,
        'options' => [
            'route' => '[/:action]',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index'
            ],
        ],
    ],
    'user' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/user[/:action][/:key]',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'login'
            ],
        ],
        'constraints' => [
            'action' => '(?!show)'
        ]
    ],
    'show-user' => [
        'type'    => Regex::class,
        'options' => [
            'regex' => '/user/(?<id>[0-9]+)',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action'     => 'show'
            ],
            'spec' => '/user/%id%'
        ]
    ],
    'recovery-password' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/recover-password/:hash',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'recover-password'
            ],
        ],
    ],
    'recovery-password-cancel' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/recover-password-cancel/:hash',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'recover-password-cancel'
            ],
        ],
    ],
];